// SPDX-License-Identifier: GPL-2.0-or-later 
// =============================================================================
// Copyright (c) <2021>, Intel Corporation
//
// This program is free software; you can redistribute it and/or modify it
// under the terms and conditions of the GNU General Public License,
// version 2, as published by the Free Software Foundation.
// 
// This program is distributed in the hope it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
// 
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =============================================================================


/**
 * @file intel_freq_control.c
 * @author VVDN Technologies
 * @brief LMK control logic for MANDAC and holdover access
 * */


/******************************************************************************
 * Include private Header files
 ******************************************************************************/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/types.h>
#include <linux/clk.h>
#include <linux/gcd.h>
#include <linux/module.h>
#include <linux/math64.h>
#include <linux/net_tstamp.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/time.h>
#include <linux/types.h>
#include <linux/kernel.h>  
#include <linux/fs.h> 
#include <linux/uaccess.h>     
#include <asm/uaccess.h> 
#include <linux/buffer_head.h>  

#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <asm/ioctls.h> 
#include <uapi/linux/kernel.h>
#include <linux/kernel.h>
#include "intel_freq_control.h"


struct freq_work {
	struct work_struct w;
	long scaled_ppm;
};

static struct freq_work new_work;

struct i2c_adapter *i2c_adap = NULL;
struct i2c_board_info i2c_info;
static struct i2c_client *i2c_cli = NULL;

static u8 i2c_lmk05028_write_byte_data(const struct i2c_client *client, u16 reg, u8 data)
{
        int status;
        u8 reg_buf[3];

        reg_buf[0] = (reg & 0xff00) >> 8;
        reg_buf[1] = (reg & 0xff);
	reg_buf[2] = data;

        struct i2c_msg msg[1] = { 
                {
                        .addr = client->addr,
                        .len = 3,
                        .buf = reg_buf,
                }
        };

        status = i2c_transfer(client->adapter, msg, 1);
        if (status < 0) {
                printk("I2C write failed .addr = %d reg_buf[0] = %d reg_buf[1] = %d reg_buf[2] = %d status : %d \n", client->addr, reg_buf[0], reg_buf[1],reg_buf[2],status);
                return status;
        }
                printk("I2C write passed .addr = %d reg_buf[0] = %d reg_buf[1] = %d reg_buf[2] = %d status : %d \n", client->addr, reg_buf[0], reg_buf[1],reg_buf[2],status);
        return 0;
}

static u8 i2c_lmk05028_read_byte_data(const struct i2c_client *client, u16 reg, u8 *buf)
{
        int status;
	u8 reg_buf[2];

	reg_buf[0] = (reg & 0xff00) >> 8;
	reg_buf[1] = (reg & 0xff);

    	struct i2c_msg msg[2] = {
        	{
            		.addr = client->addr,
            		.len = 2,
            		.buf = reg_buf,
        	},
        	{
            		.addr = client->addr,
            		.flags = I2C_M_RD,
            		.len = 1,
            		.buf = buf,
        	}
    	};

        status = i2c_transfer(client->adapter, msg, 2);
        if (status < 0) {
                printk("I2C read failed .addr = %d reg_buf[0] = %d reg_buf[1] = %d buf = %d status : %d", client->addr, reg_buf[0], reg_buf[1],*buf,status);
        	return status;
    	}
                printk("I2C read passed .addr = %d reg_buf[0] = %d reg_buf[1] = %d buf = %d status : %d", client->addr, reg_buf[0], reg_buf[1],*buf,status);
	return 0;
} 



static void lmk_05028_freq_control(struct work_struct *work)
{
	short int i;
	long scaled_ppm;
	u8 temp, num_reg 	 = 0x68;        //PLL1_NUM
	u16 post_divide_reg 	 = 0x5c;       	//P1PLL1
	u16 fdev_reg_update	 = 0x24b;       //DPLL1_FDEV_REG_CTL
	u16 fdev_reg 		 = 0x24a;       //FDEV_NUM
	u16 prescaler_divide_reg = 0x169;    	//PRREF
	u16 ref_div_reg 	 = 0x133;       // DPLL1_REF0_RDIV
	u16 ref_den_reg 	 = 0x174;       //DPLL1_REF_DEN
	u32 fIN 		 = 390625000;   //in hz
	u32 fVCO 		 = 390625000;   // in hz
	u8 rIN, post_div_val, prescaler_div_val;
	s32 status;
	s64 step_val, step_val_2;
       	u64 denominator;
	char i2c_den_reg;
	struct freq_work *p_work;
	p_work = container_of(work, struct freq_work, w);
	
	if(determine_i2c_client(1,0x60,"lmk05028")){
		i2c_lmk05028_read_byte_data(i2c_cli, ref_div_reg, &rIN);

		i2c_lmk05028_read_byte_data(i2c_cli,post_divide_reg,&post_div_val);
		post_div_val &= 0xf;

		i2c_lmk05028_read_byte_data(i2c_cli,post_divide_reg,&prescaler_div_val);
		prescaler_div_val &= 0xf;
	}else if(determine_i2c_client(0,0x6c,"si5383")){
		si_5383evb_freq_control(work);
		return;
	}

	scaled_ppm =  p_work->scaled_ppm;
	//scaled_ppb *= 125;
	//scaled_ppb >>= 13;
	for(i=0, fdev_reg = 0x246; i<5; i++,fdev_reg++){
		i2c_lmk05028_read_byte_data(i2c_cli,fdev_reg, &temp);
		step_val |= (temp & 0xff);
		step_val = step_val << 8;
	}
	fdev_reg = 0x24a;
	
	printk("Step_val read_function Current Numerator value before writing : %lld", (u64)step_val);

	//Read Numerator and Denominator value of PLL1
	for(i=0, denominator=0,temp=0; i<5 ; i++,num_reg++,ref_den_reg++){
		i2c_lmk05028_read_byte_data(i2c_cli,ref_den_reg,&temp);
		denominator |= temp;
		denominator <<= 8;
		i2c_lmk05028_read_byte_data(i2c_cli,ref_den_reg,&i2c_den_reg);
		printk("Denominator iteration : %d value : %lld, i2c_den_reg:%d",i,denominator,i2c_den_reg);
	}
	
	switch(post_div_val)
        {
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 0xa:
		case 0xc:
                        post_div_val+=1;
			break;
                default:
                        printk("not a valid primary post-divide value ");
			post_div_val = 4; // CAUTION: Added for debugging, to avoid divide by 0
        }

	/*printk("\nscaled ppb:%ld denominator:%lld fIN:%d RIN:%d fVCO:%d post divide value:%d presacle divider value:%d",scaled_ppb,denominator,fIN,rIN,fVCO,post_div_val,prescaler_div_val);*/

	step_val = ((s64)scaled_ppm / 10^6) * (denominator / ( fIN / rIN )) * (fVCO / ( post_div_val * prescaler_div_val ));
	printk("Step_val write_function Step Value after calculation 1 and writing : %lld", step_val);
	step_val_2 = ((s64)scaled_ppm ) * (denominator / ( fIN / rIN )) * (fVCO / ( post_div_val * prescaler_div_val ));
	printk("Step_val write_function Step Value after calculation 2 and writing : %lld", step_val_2);
	//step_val /= 10^6; //10^9 from LMK equation for PPB its scaled down by 10^3 for PPM
	step_val /= 8192;

	printk("\n Calculated step_value:%lld rIN : %d denominator:%d scaled_ppm: %d prescaler_div_val:%d, post_div_val:%d",step_val, rIN, denominator, scaled_ppm,prescaler_div_val, post_div_val);

	printk("Step_val write_function Step Value after calculation 3 and writing : %lld", step_val);
	//Write new step value into DPLL1_FDEV registers
	for(i=5,step_val=0; i<0; i--,fdev_reg--){
		temp = step_val & 0xff;
		i2c_lmk05028_write_byte_data(i2c_cli,fdev_reg,temp);
		step_val >>= 8;
	}
	//printk("Step_val write_function after calculation and writing : %lld", step_val);

	if(scaled_ppm < 0)
		i2c_lmk05028_write_byte_data(i2c_cli,fdev_reg_update,1);
	else
		i2c_lmk05028_write_byte_data(i2c_cli,fdev_reg_update,0);
	
	return;
}

static int determine_i2c_client(int adapter,int addr,char *type)
{
	if(i2c_cli)
		return 1; //I2C client already present

	i2c_adap = i2c_get_adapter(adapter);
	memset(&i2c_info, 0, sizeof(struct i2c_board_info));
	strlcpy(i2c_info.type, type, I2C_NAME_SIZE);
	i2c_info.addr = addr;
	i2c_cli = i2c_new_device(i2c_adap,&i2c_info);
	if(i2c_cli == NULL)
		return 0; 
	else
		return 1;

}
//si 5383evb frequency control
static void si_5383evb_freq_control(struct work_struct *work)
{
	long scaled_ppm;
	u64 step_val;
	u8 data1,data2,data3,data4,data5,data6,data7;
	s32 resd1_rd;
	s64 m_fstepw_plla;
	s32 remainder;
	u64 m_num_plla=55364812800000;


	struct freq_work *p_work;
        p_work = container_of(work, struct freq_work, w);

	scaled_ppm = p_work->scaled_ppm;
	//s64 ppb_new = ((s64)(ppb));
	
	if(scaled_ppm==0)
	{
		step_val=m_num_plla;
	}
	else
	{
		//m_fstepw_plla	=	div_s64_rem((s64)(55364812800000 * scaled_ppm), 65536000000,&remainder);
		m_fstepw_plla	=	div_s64_rem((s64)(553648128 * (s64)scaled_ppm), 655360,&remainder);
		step_val = m_num_plla + m_fstepw_plla;
	}
	//byte extraction
	data1=((u8)(step_val	&	0x0000000000ff));
	data2=((u8)((step_val	&	0x00000000ff00)>>8));
	data3=((u8)((step_val	&	0x000000ff0000)>>16));
	data4=((u8)((step_val	&	0x0000ff000000)>>24));
	data5=((u8)((step_val	&	0x00ff00000000)>>32));
	data6=((u8)((step_val	&	0xff0000000000)>>40));
	data7=((u8)(step_val	&	0x000000000000));


	resd1_rd=i2c_smbus_read_byte_data(i2c_cli, 0xFE);
	u8 reg1 = 0x20; /* Device register to access */
	u8 reg2 = 0x21;
	u8 reg3 = 0x2A;
	u8 regd1 = 0x22;
	u8 regd2 = 0x23;
	u8 regd3 = 0x24;
	u8 regd4 = 0x25;
	u8 regd5 = 0x26;
	u8 regd6 = 0x27;
	u8 regd7 = 0x28;
	s32 res1,res2,res3,resd1,resd2,resd3,resd4,resd5,resd6,resd7;
	res1 = i2c_smbus_write_byte_data(i2c_cli, reg1,0x04);
	res2 = i2c_smbus_write_byte_data(i2c_cli, reg2,0x15);
	resd1= i2c_smbus_write_byte_data(i2c_cli, regd1,data1);
	resd2= i2c_smbus_write_byte_data(i2c_cli, regd2,data2);
	resd3= i2c_smbus_write_byte_data(i2c_cli, regd3,data3);
	resd4= i2c_smbus_write_byte_data(i2c_cli, regd4,data4);
	resd5= i2c_smbus_write_byte_data(i2c_cli, regd5,data5);
	resd6= i2c_smbus_write_byte_data(i2c_cli, regd6,data6);
	resd7= i2c_smbus_write_byte_data(i2c_cli, regd7,data7);
	res3 = i2c_smbus_write_byte_data(i2c_cli, reg3,0x64);
}

int freq_control(long scaled_ppm)
{
	new_work.scaled_ppm = scaled_ppm;
	INIT_WORK(&new_work.w,lmk_05028_freq_control);

	if(schedule_work(&new_work.w) == 0){
		printk("Schedule lmk_freq work failed");
		return -1;
	}
	else{
		return 0;
	}
}
